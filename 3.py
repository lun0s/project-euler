#!/usr/bin/python3
from math import sqrt

#Number: 3
#Name: 
Description = """The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?"""

def isPrime(inputNum): 
  #Source: https://en.wikipedia.org/wiki/Primality_test#Pseudocode
  #Used the pseudocode to build this function
  if inputNum % 2 == 0 or inputNum % 3 == 0:
    return False
  i = 5
  while i * i <= inputNum:
    if inputNum % i == 0 or inputNum % (i+2) == 0:
      return False
    i = i + 6
  return True
  
numCheck = 600851475143
primeCheck = False
sqRoot = int(sqrt(numCheck))

while primeCheck is False:
  if numCheck % sqRoot == 0 and isPrime(sqRoot) == True:
    primeCheck = True
  else:
    sqRoot -= 1
print(sqRoot)