#!/usr/bin/python3

#Number: 4
#Name: Largest palindrome product
Description = """A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers."""

palCheck = False
numA = 999
numB = 999
while palCheck is not True:
  numB -= 1
  for x in range(1,11):
    numSum = (numA-x)*numB
    if str(numSum) == str(numSum)[::-1]:
      palCheck = True
      numA = numA-x
      break

print(numA, "*", numB, "=", numSum)