#!/usr/bin/python3

#Number: 5
#Name: Smallest multiple
Description = """
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
"""

startNum = 0
endLoop = False
while endLoop == False:
  startNum += 20
  for i in range(1,21):
    if startNum % i == 0:
      endLoop = True
    else:
      endLoop = False
      break
print("Answer:",startNum)