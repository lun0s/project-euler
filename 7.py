#!/usr/bin/python3

#Number: 7
#Name: 10001st prime
Description = """
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
"""

def isPrime(inputNum): 
  #Source: https://en.wikipedia.org/wiki/Primality_test#Pseudocode
  #Used the pseudocode to build this function
  if inputNum % 2 == 0 or inputNum % 3 == 0:
    return False
  i = 5
  while i * i <= inputNum:
    if inputNum % i == 0 or inputNum % (i+2) == 0:
      return False
    i = i + 6
  return True

endPrime = 10001
curPrime = 2

i = 3
while curPrime < endPrime:
  i += 2
  if isPrime(i):
    curPrime += 1
    
print(i)